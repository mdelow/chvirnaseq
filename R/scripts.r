setwd("C://Users/Maya/Dropbox/chvIrnaseq/R/")

#create a plot of the number of significantly differentially expressed genes in the chvI mutants compared to the wildtype in LBMC
#and MM9 conditions

#load libraries
library(ggplot2)
library(reshape2)
library(plyr)


#read in dataset
count <- read.csv("chvIcountdata.csv", header=T)

#melt the data table
count.m <- melt(count, id.vars = c("strain", "condition"))
#assign factors
count.m$strain <- factor(count.m$strain, as.character(count.m$strain))

#plot using geom_bar in ggplot
ggplot(count.m, aes(strain), ylim(-1700:1700)) + 
  geom_bar(data = subset(count.m, variable == "count.up"), 
           aes(y = value, fill = condition), stat = "identity", position = "dodge") +
  geom_bar(data = subset(count.m, variable == "count.down"), 
           aes(y = -value, fill = condition), stat = "identity", position = "dodge") + 
  geom_hline(yintercept = 0,colour = "grey90")

#add labels to the bars
last_plot() + 
  geom_text(data = subset(count.m, variable == "count.up"), 
            aes(strain, value, group=condition, label=value),
            position = position_dodge(width=0.9), vjust = 1.5, size=4) +
  geom_text(data = subset(count.m, variable == "count.down"), 
            aes(strain, -value, group=condition, label=value),
            position = position_dodge(width=0.9), vjust = -.5, size=4) +
  coord_cartesian(ylim = c(-1700, 1700))

#make the plot pretty
last_plot()+
  ylab("Number of genes") +
  theme_classic()+
  theme(axis.line.x=element_blank())+
  theme(axis.title.x=element_blank())+
  theme(axis.ticks.x=element_blank())+
  scale_fill_brewer(palette="Paired")
  
#to make a Venn diagram of the datasets in the two conditions 
#load libraries
library(VennDiagram)
library(RColorBrewer)

#bring in list of significant genes for each condition
MM9genes <- readLines("MM9siggenes.csv")
LBMCgenes <- readLines("LBMCsiggenes.csv")

#get a list of genes that overlap between the two conditions
chvIlist <- intersect(MM9genes, LBMCgenes)
#get length of the above list
chvIlen <- length(chvIlist)

#get the length of the number of genes differentially expressed in MM9
MM9geneslen <- (length(MM9genes))
#get the length of the number of genes differentially expressed in LBMC
LBMCgeneslen <- (length(LBMCgenes))

#make the Venn diagram, plotting the length of each of the three lists
grid.newpage()
draw.pairwise.venn(MM9geneslen, LBMCgeneslen, chvIlen, category = c("MM9", "Nutrient Shift"), lty = rep("blank", 2), fill = c("#7570b3", "#1b9e77"), alpha = rep(0.5, 2), cat.pos = c(0,0), cat.dist = rep(0.025, 2))

#output list of genes unique to LBMC
LBMCunique <- setdiff(LBMCgenes, MM9genes)
writeLines(unlist(LBMCunique), "LBMConly.txt")

#same as above, unique to MM9
MM9unique <- setdiff(MM9genes, LBMCgenes)
writeLines(unlist(MM9unique), "MM9only.txt")

#shared genes
shared <- intersect(MM9genes, LBMCgenes)
writeLines(unlist(shared), "sharedgenes.txt")

#create clustered heatmap of the transcriptional effects of chvI mutation in LBMC and MM9, looking only at genes with a significant
#difference in expression in at least one of the conditions




list <- union(MM9genes,LBMCgenes)

#read in files with log2 fold change comparisons for the chvI mutant, compared to the wildtype
#for all genes in the two conditions
MM9comp <- read.csv("MM9.csv",header=T, skip=0, stringsAsFactors=F)
LBMCcomp <- read.csv("LBMC.csv",header=T, skip=0, stringsAsFactors=F)

#select only the genes in the list, and bring their log2 fold change values
MM9unionvalues <- subset(MM9comp, MM9comp[,2] %in% list)
LBMCunionvalues <- subset(LBMCcomp, LBMCcomp[,2] %in% list)

#sort data tables by gene
MM9unionvalues <- MM9unionvalues[order(MM9unionvalues[,"gene_id"]),]
LBMCunionvalues <- LBMCunionvalues[order(LBMCunionvalues[,"gene_id"]),]

#create single table with only log2 values from each condition
col <- "log2"
unionplot <- cbind(MM9unionvalues[,col], LBMCunionvalues[,col])
rownames(unionplot) <- MM9unionvalues[,"gene_id"]
colnames(unionplot) <- c("MM9log2", "LBMClog2")
unionplot<-as.data.frame(unionplot)



# hierarchical clustering with hclust
#to see the cluster dendrogram
plot(hclust(dist(unionplot)))
#create hclust object
hc <- hclust(dist(unionplot))
#cut tree at specific height and sort in to groups
groups <- cutree(hc, h = 6)








#assign groups to original dataframe
x <- cbind(unionplot, groups)

#pull out subset groups
group1 <- subset(unionplot, groups==1)
group2 <- subset(unionplot, groups==2)
group3 <- subset(unionplot, groups==3)
group4 <- subset(unionplot, groups==4)
group5 <- subset(unionplot, groups==5)
group6 <- subset(unionplot, groups==6)
group7 <- subset(unionplot, groups==7)
group8 <- subset(unionplot, groups==8)


#output .txt files of gene names for each group

names1 <- rownames(group1)
writeLines(unlist(names1), "group1.txt")

names2 <- rownames(group2)
writeLines(unlist(names2), "group2.txt")

names3 <- rownames(group3)
writeLines(unlist(names3), "group3.txt")

names4 <- rownames(group4)
writeLines(unlist(names4), "group4.txt")

names5 <- rownames(group5)
writeLines(unlist(names5), "group5.txt")

names6 <- rownames(group6)
writeLines(unlist(names6), "group6.txt")

names7 <- rownames(group7)
writeLines(unlist(names7), "group7.txt")

names8 <- rownames(group8)
writeLines(unlist(names8), "group8.txt")



unionplot$MM9log2 <- as.numeric(as.character(unionplot$MM9log2))
unionplot$LBMClog2 <- as.numeric(as.character(unionplot$LBMClog2))

unionmatrix <-data.matrix(unionplot)

palette <- colorRampPalette(c("grey42", "white", "turquoise4"))



library("gplots")
heatmap.2(unionmatrix, scale="none", col=palette, ColV=FALSE, key=TRUE, symkey=FALSE, labRow=FALSE, density.info="none", trace="none", cexRow=0.5)






#make Manhattan like plot to visualize the transcriptional differences
#of genes across the genome

#remove any NAs
MM9comp<-na.omit(MM9comp)
LBMCcomp<-na.omit(LBMCcomp)

#order the two tables
MM9manhattan <- MM9comp[order(MM9comp[,"gene_id"]),]
LBMCmanhattan <- LBMCcomp[match(MM9comp[,"gene_id"],LBMCcomp[,"gene_id"]),]

#organize in to a single data table with log2 fold change only
col <- "log2"
manhattan <- cbind(MM9manhattan[,col],LBMCmanhattan[,col])
colnames(manhattan) <- c("MM9log2","LBMClog2")
rownames(manhattan) <- MM9manhattan[,"gene_id"]

#read in the genome coordinate data
chrom.coords <- read.delim("chrom.coords",sep='\t',header=F, stringsAsFactors=F)
psymA.coords <- read.delim("psymA.coords",sep='\t',header=F, stringsAsFactors=F)
psymB.coords <- read.delim("psymB.coords",sep='\t',header=F, stringsAsFactors=F)
coords <- rbind(psymA.coords,psymB.coords,chrom.coords)

#removing the genes without coordinates from the log_matr data
manhattan <- manhattan[rownames(manhattan) %in% coords$V2,]

#removing the genes without log2 values from the coords data
coords2 <- coords[coords$V2 %in% rownames(manhattan),]

#organizing and order the coordinates
coords <- coords2[,1]
coordorder <-coords2[,2]

#matching the order in the table
manhattan <- manhattan[match(coordorder, rownames(manhattan)),]

#set the y-limits of the plot
yl <- c(-11,8)
xl <-c(0, 6690750)

#create points for plots, getting x and y values
pointCols <- vector(length = nrow(coords2))
pointCols[min(grep("SMa",coords2$V2)):max(grep("SMa",coords2$V2))] <- "dark blue"
pointCols[min(grep("SMb",coords2$V2)):max(grep("SMb",coords2$V2))] <- "dark green"
pointCols[min(grep("SMc",coords2$V2)):max(grep("SMc",coords2$V2))] <- "dark red"

#plot MM9 manhattan
par(mfrow=c(1,1),mar=c(2, 4, 0, 2) + 1)
plot(coords2[,1], manhattan[,1],cex=0.2,ylim=yl,col=pointCols, xlim=xl)
#add gene labels to plot if desired
text(coords2[,1], manhattan[,1], labels=rownames(manhattan), cex= 0.1)

#plot LBMC manhattan
par(mfrow=c(1,1),mar=c(2, 4, 0, 2) + 1)
plot(coords2[,1], manhattan[,2],cex=0.2,ylim=yl,col=pointCols, xlim=xl)
text(coords2[,1], manhattan[,2], labels=rownames(manhattan), cex= 0.1)

#plot both together, MM9 on top
par(mfrow=c(2,1),mar=c(2, 4, 0, 2) + 1)
plot(coords2[,1], manhattan[,1],cex=0.2,ylim=yl,col=pointCols, xlim=xl)
plot(coords2[,1], manhattan[,2],cex=0.2,ylim=yl,col=pointCols, xlim=xl)


#comparing our data to the chen data set
#to get values from the charleslab data to correspond to the genes from the chen study
#bring in data from the chen study
chentable <- read.csv("chendata.csv", header=F, stringsAsFactors=F, row.names=1)
colnames(chentable) <- c("SLR")

#create list of genes in the chen study
chenlist <- row.names(chentable)

#create subset of LBMC charles lab data based on genes in the chen study
charlestable <- subset(LBMCcomp, LBMCcomp$gene_id %in% chenlist )


#write out the data
write.csv(charlestable, file ="charlestableforchen.csv")

##to make a comparison plot of charles and chen data
#read in data
comparison <- read.csv("chencharlescomparison.csv", header=F, stringsAsFactors=F, row.names=1)
colnames(comparison) <- c("chen","charles")

#plot in ggplot
ggplot(comparison, aes(x=charles, y=chen))+
  geom_point() +
  geom_smooth(method = "lm")

cor(comparison$charles, comparison$chen)
cor.test(comparison$charles, comparison$chen)


#heathers gene list log2 vs all others
#bring in list of heather's genes
heather <- read.csv("heathersgenes.csv", header=F, stringsAsFactors=F)
#make list
hgenes <- heather$V1
#subset of data specific to genes in hgenes
hMM9 <- subset(MM9comp, MM9comp$test_id %in% hgenes)
hLBMC <- subset(LBMCcomp, LBMCcomp$test_id %in% hgenes)

#sort subsetted data by gene
hMM9 <- hMM9[order(hMM9[,"gene_id"]),]
hLBMC <- hLBMC[order(hLBMC[,"gene_id"]),]

#combine together
hlog <- cbind(hMM9$log2, hLBMC$log2)
colnames(hlog) <- c("MM9", "LBMC")
rownames(hlog) <- hMM9$test_id
hlog <- as.data.frame(hlog)
hlog$group <- 'heather'

#subset the background data that excludes heathers genes
#some genes were removed from LBMC and MM9 base files due to inf or na values,
#these differ between the two datasets, need to match them
commongenes <- intersect(MM9comp$gene_id, LBMCcomp$gene_id)
MM9common <- subset(MM9comp, MM9comp$gene_id %in% commongenes)
LBMCcommon <- subset(LBMCcomp, LBMCcomp$gene_id %in% commongenes)

backgroundgenes <- setdiff(MM9common$gene_id, hMM9$gene_id)
bMM9 <- subset(MM9common, MM9common$gene_id %in% backgroundgenes)
bLBMC <- subset(LBMCcommon, LBMCcommon$gene_id %in% backgroundgenes)
#sort these tables
#sort subsetted data by gene
bMM9 <- bMM9[order(bMM9[,"gene_id"]),]
bLBMC <- bLBMC[order(bLBMC[,"gene_id"]),]


#bring together
blog <- cbind(bMM9$log2, bLBMC$log2)
colnames(blog) <- c("MM9", "LBMC")
rownames(blog) <- bMM9$test_id
blog <- as.data.frame(blog)
blog$group <- 'background'

#combine in to one dataset
together <- rbind(hlog, blog)

#reading out
write.csv(together, "together.csv")
#read back in to avoid issues with data types
together <- read.csv("together.csv", stringsAsFactors=F, header=T, row.names=1)

#compare groups

ggplot(together, aes(x = group, y = MM9)) +
  geom_boxplot()

ggplot(together, aes(x = group, y = LBMC)) +
  geom_boxplot()

#stat analysis
library(agricolae)
stat.aov <- aov(LBMC~group, data=together)
summary(stat.aov)
TukeyHSD(stat.aov, conf.level=0.95)
model<-lm('LBMC~group', data=together)
do<-HSD.test(model,"group", group=TRUE)
do

stat.aov <- aov(MM9~group, data=together)
summary(stat.aov)
TukeyHSD(stat.aov, conf.level=0.95)
model<-lm('MM9~group', data=together)
do<-HSD.test(model,"group", group=TRUE)
do

#examining previously identified target genes in our data set
#read in list of identified targets
targets <- scan("identifiedtargets.txt", what="", sep="\n")

#make data subset of identified targets data
targetMM9 <- subset(MM9common, MM9common$gene_id %in% targets)
targetLBMC <- subset(LBMCcommon, LBMCcommon$gene_id %in% targets)

#sort subsetted data by gene
targetMM9 <- targetMM9[order(targetMM9[,"gene_id"]),]
targetLBMC <- targetLBMC[order(targetLBMC[,"gene_id"]),]


#bring together
targetdata <- cbind(targetMM9$log2, targetLBMC$log2)
colnames(targetdata) <- c("MM9", "LBMC")
rownames(targetdata) <- targetMM9$test_id
targetdata <- as.data.frame(targetdata)
targetdata$group <- 'target'


#make data subset of everything else
nontargetgenes <- setdiff(MM9common$gene_id, targets)
ntMM9 <- subset(MM9common, MM9common$gene_id %in% nontargetgenes)
ntLBMC <- subset(LBMCcommon, LBMCcommon$gene_id %in% nontargetgenes)
#sort these tables
#sort subsetted data by gene
ntMM9 <- ntMM9[order(ntMM9[,"gene_id"]),]
ntLBMC <- ntLBMC[order(ntLBMC[,"gene_id"]),]


#bring together
ntdata <- cbind(ntMM9$log2, ntLBMC$log2)
colnames(ntdata) <- c("MM9", "LBMC")
rownames(ntdata) <- ntMM9$test_id
ntdata <- as.data.frame(ntdata)
ntdata$group <- 'background'

#combine in to one dataset
targetalldata <- rbind(targetdata, ntdata)

#reading out
write.csv(targetalldata, "targetalldata.csv")
#read back in to avoid issues with data types
targetalldata <- read.csv("targetalldata.csv", stringsAsFactors=F, header=T, row.names=1)

##plot difference

ggplot(targetalldata, aes(x = group, y = MM9)) +
  geom_boxplot()

ggplot(targetalldata, aes(x = group, y = LBMC)) +
  geom_boxplot()

#stat analysis
library(agricolae)
stat.aov <- aov(LBMC~group, data=targetalldata)
summary(stat.aov)
TukeyHSD(stat.aov, conf.level=0.95)
model<-lm('LBMC~group', data=targetalldata)
do<-HSD.test(model,"group", group=TRUE)
do

stat.aov <- aov(MM9~group, data=targetalldata)
summary(stat.aov)
TukeyHSD(stat.aov, conf.level=0.95)
model<-lm('MM9~group', data=targetalldata)
do<-HSD.test(model,"group", group=TRUE)
do








