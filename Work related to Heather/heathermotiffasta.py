import csv


#opens a csv file that has the motif seq and a number in another column
input = open('uniquefimomatcheswholegenome.csv', 'r')


input_data = csv.reader(input)

file=open('heathermotiffimogenome.txt', 'w')

#takes each row, adds >motif to conform to .fasta format
for row in input_data:
	file.write(">motif" + row[0] + "\n" + row[1] + "\n")