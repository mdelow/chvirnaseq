
import csv
import sys

genelist = open(sys.argv[1], "r")
genelist_data=csv.reader(genelist)

for row in genelist_data:
	#Read in the file
	with open(sys.argv[2], 'r') as file:
		filedata = file.read()
	# Replace the target string
	filedata = filedata.replace(str(row[0]), str(row[1]))
	# Write the file out again
	with open(sys.argv[2], 'w') as file:
		file.write(filedata)
	file.close()