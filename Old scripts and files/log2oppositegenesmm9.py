#get log2 and annotation for opposite genes

outfile = open('log2oppositegenesall.csv','w')
import csv

list = open('log2oppositegenesLBMC.csv', 'r')
list_data = csv.reader(list)
mm9 = open('+chvI.csv', 'r')
mm9_data = csv.reader(mm9)

for row_list in list_data:
	for row_mm9 in mm9_data:
		if row_list[0]==row_mm9[1]:
			outfile.write(row_list[0] + "," + row_list[1] + "," + row_list[2] + "," + row_mm9[10] + "," + row_mm9[14] + "\n")
	mm9.seek(0)	