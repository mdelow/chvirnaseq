#add log2 then get annotation

import csv
import sys

file1 = open(sys.argv[1], "r")
file2 = open(sys.argv[2], "r")
file1_data = csv.reader(file1)
file2_data = csv.reader(file2)

#makes new blank txt file
file = open(sys.argv[1] + "log2" + ".txt","w")


#comparing rows in file 2 to the list of genes from file1
for row_1 in file1_data:
	for row_2 in file2_data:
		if row_1[0] == row_2[1]:
			file.write(row_1[0]+"\t"+row_2[2] + "\n")
	file2.seek(0)