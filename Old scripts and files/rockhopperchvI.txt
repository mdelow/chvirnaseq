java -Xmx1200m -cp Rockhopper.jar Rockhopper

java Rockhopper <options> -g genome_DIR1,genome_DIR2 aerobic_replicate1_pairedend1.fastq%aerobic_replicate1_pairedend2.fastq,aerobic_replicate2_pairedend1.fastq%aerobic_replicate2_pairedend2.fastq anaerobic_replicate1_pairedend1.fastq%anaerobic_replicate1_pairedend2.fastq,anaerobic_replicate2_pairedend1.fastq%anaerobic_replicate2_pairedend2.fastq

java Rockhopper -g  -o output -v -y - t HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz%HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R2.fastq.gz,HI.1305.006.Index_11.34Rm1021WTMM9Rep2_R1.fastq.gz%HI.1305.006.Index_11.34Rm1021WTMM9Rep2_R2.fastq.gz HI.1305.006.Index_14.37SmUW38Tn5chvIMM9Rep1_R1.fastq.gz%HI.1305.006.Index_14.37SmUW38Tn5chvIMM9Rep1_R2.fastq.gz, HI.1305.006.Index_15.38SmUW38Tn5chvIMM9Rep2_R1.fastq.gz%HI.1305.006.Index_15.38SmUW38Tn5chvIMM9Rep2_R2.fastq.gz




java Rockhopper -g  -o output -v -y - t HI.1305.006.Index_12.35Rm1021WTLBmcRep1_R1.fastq.gz%HI.1305.006.Index_12.35Rm1021WTLBmcRep1_R2.fastq.gz, HI.1305.006.Index_13.36Rm1021WTLBmcRep2_R1.fastq.gz%HI.1305.006.Index_13.36Rm1021WTLBmcRep2_R2.fastq.gz HI.1305.006.Index_16.39SmUW38Tn5chvILBmcRep1_R1.fastq.gz%HI.1305.006.Index_16.39SmUW38Tn5chvILBmcRep1_R2.fastq.gz, HI.1305.006.Index_18.40SmUW38Tn5chvILBmcRep2_R1.fastq.gz%HI.1305.006.Index_18.40SmUW38Tn5chvILBmcRep2_R2.fastq.gz


wget --timestamping ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/Sinorhizobium_meliloti/latest_assembly_versions/GCA_000006965.1_ASM696v1/GCA_000006965.1_ASM696v1_cds_from_genomic.fna.gz -P genome/
wget --timestamping ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/Sinorhizobium_meliloti/latest_assembly_versions/GCA_000006965.1_ASM696v1/GCA_000006965.1_ASM696v1_feature_table.txt.gz -P genome/
wget --timestamping ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/Sinorhizobium_meliloti/latest_assembly_versions/GCA_000006965.1_ASM696v1/GCA_000006965.1_ASM696v1_genomic.fna.gz -P genome/
wget --timestamping ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/Sinorhizobium_meliloti/latest_assembly_versions/GCA_000006965.1_ASM696v1/GCA_000006965.1_ASM696v1_genomic.gbff.gz -P genome/
wget --timestamping ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/Sinorhizobium_meliloti/latest_assembly_versions/GCA_000006965.1_ASM696v1/GCA_000006965.1_ASM696v1_genomic.gff.gz -P genome/
wget --timestamping ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/Sinorhizobium_meliloti/latest_assembly_versions/GCA_000006965.1_ASM696v1/GCA_000006965.1_ASM696v1_protein.faa.gz -P genome/
wget --timestamping ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/Sinorhizobium_meliloti/latest_assembly_versions/GCA_000006965.1_ASM696v1/GCA_000006965.1_ASM696v1_protein.gpff.gz -P genome/
wget --timestamping ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/Sinorhizobium_meliloti/latest_assembly_versions/GCA_000006965.1_ASM696v1/GCA_000006965.1_ASM696v1_rna_from_genomic.fna.gz -P genome/
