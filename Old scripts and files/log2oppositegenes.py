#get log2 and annotation for opposite genes

outfile = open('log2oppositegenesLBMC.csv','w')
import csv

list = open('oppositegenes.csv', 'r')
list_data = csv.reader(list)
lbmc = open('-chvI.csv', 'r')
lbmc_data = csv.reader(lbmc)

for row_list in list_data:
	for row_lbmc in lbmc_data:
		if row_list[0]==row_lbmc[1]:
			outfile.write(row_list[0] + "," + row_lbmc[10] + "," + row_lbmc[14] + "\n")
	lbmc.seek(0)	