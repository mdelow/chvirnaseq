#to find genes that are effected in opposite directions in LBMC and MM9

outfile1 = open('oppositegenes1.csv', 'w')
outfile2 = open('oppositegenes2.csv', 'w')

import csv

mm9 = open('+chvI.csv', 'r')
lbmc = open('-chvI.csv', 'r')
mm9_data = csv.reader(mm9)
lbmc_data = csv.reader(lbmc)

mm9pos=[]
mm9neg=[]
mm9unknown=[]


for row_mm9 in mm9_data:
	value=float(row_mm9[10])
	if value > 0:
		mm9pos.append(row_mm9[1])
	elif value < 0:
		mm9neg.append(row_mm9[1])
	else: #row_mm9 == 0
		mm9unknown.append(row_mm9[1])

lbmcpos=[]
lbmcneg=[]
lbmcunknown=[]


for row_lbmc in lbmc_data:
	value=float(row_lbmc[10])
	if value > 0:
		lbmcpos.append(row_lbmc[1])
	elif value < 0:
		lbmcneg.append(row_lbmc[1])
	else: #row_lbmc == 0
		lbmcunknown.append(row_lbmc[1])


one = list(set(mm9pos).intersection(lbmcneg))
two = list(set(lbmcpos).intersection(mm9neg))

outfile1.write("\n".join(one))	
outfile2.write("\n".join(two))
