#select genes that are significant in both conditions and in opposite directions

outfile = open('doubleyesopposite.csv','w')
import csv

list = open('log2oppositegenesall.csv', 'r')
list_data = csv.reader(list)


for row_list in list_data:
	if row_list[2]=='yes' and row_list[4]=='yes':
		outfile.write(row_list[0] + "," + row_list[1] + "," + row_list[2] + "," + row_list[3] + "," + row_list[4] + "\n")

		
outfile.close()