#to combine log2 values from original (OG) analysis and new RNAseq analysis to determine similarity/difference between the two attempts
import csv
import sys

#make a list of the shared genes
gene = []
with open("coregenelists.txt") as f:
    for line in f:
       gene.append(line)
gene = [x.strip('\n') for x in gene]
gene = [x.strip() for x in gene]

print gene
#make output file
outfile1 = open(sys.argv[1]+'sharedlog2.csv','w')
outfile2 = open(sys.argv[1]+'uniquelog2.csv','w')

#read in data
log2 = open(sys.argv[1], 'r')	   
log2_data = csv.reader(log2)


for row_log2 in log2_data:
	match=False
	for item in gene:
		if str(row_log2[1])==item:
			match=True
			break
	if match==True:
		outfile1.write(row_log2[1] + "," + row_log2[9] + "," + row_log2[13] + "\n")
	else:
		outfile2.write(row_log2[1] + "," + row_log2[9] + "," + row_log2[13] + "\n")		
	
outfile1.close()
outfile2.close()