PASS	Basic Statistics	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
PASS	Per base sequence quality	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
PASS	Per sequence quality scores	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
FAIL	Per base sequence content	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
FAIL	Per base GC content	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
FAIL	Per sequence GC content	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
PASS	Per base N content	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
PASS	Sequence Length Distribution	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
FAIL	Sequence Duplication Levels	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
WARN	Overrepresented sequences	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
WARN	Kmer Content	HI.1305.006.Index_10.33Rm1021WTMM9Rep1_R1.fastq.gz
