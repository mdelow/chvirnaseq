##to remove motifs from the motif annotations that occur inside of ORFS

import csv

#makes new blank txt file
outfile1 = open("motiftormchromctrA.txt","w")

#opens file with original motif annotation
file1 = open("motifingenechromctrA.csv", "r")
file1_data = csv.reader(file1)
 
#writes new file that only contains the number and start of motifs in genes
for row_1 in file1_data:
	if row_1[2]=="yes":
		outfile1.write(row_1[0]+"\t"+row_1[1]+"\n")

outfile1.close()


#read in list of motifs to remove as dict
rm = {}
with open("motiftormchromctrA.txt") as f:
    for line in f:
       (key, val) = line.split()
       rm[int(key)] = int(val)

#read in whole list of motif annotations as dict
whole = {}	   
with open("chrommotifcoordctrA.txt") as q:
    for line in q:
       (key, val) = line.split()
       whole[int(key)] = int(val)

#make list of keys from rm dictionary	   
rmlist = rm.keys()


#remove items in whole dictionary if they match items in rmlist
for x in rmlist:
	whole.pop(x)


with open("chrommotifcoordrmctrA.txt", "w") as outfile2:
	for k, v in whole.items():
		outfile2.write('%s\t%s\n' % (k, v))

