#to find closest gene to motif with reasonable upstream region - July 27 2017

import csv

#makes new blank txt file
file = open("distbetweenchromctrA.csv","w")

file1 = open("chromgenecoordsfinal.csv", "r")


file1_data = csv.reader(file1)

d = {}
with open("chrommotifcoordrmctrA.txt") as f:
    for line in f:
       (key, val) = line.split()
       d[int(key)] = int(val)
	   

for key,value in d.iteritems():
	gene = ""
	start = ""
	stop = ""
	motif = str(key)
	mstart = str(value)
	posbest = 100000000000
	temp_posbest = 99999999999
	for row_1 in file1_data:
		#looking only at positive strand
		if row_1[3] == "pos":
			#calculating distance value
			temp_posbest = int(row_1[1])-value
			#compare to best value found so far
			if temp_posbest < posbest and temp_posbest > 0:
				#reset best value
				posbest = temp_posbest
				gene = row_1[0]
				start = row_1[1]
				stop = row_1[2]
	file1.seek(0)
		#write to file
	file.write(motif + "," + mstart + "," + gene + "," + start + "," + stop + "," + str(posbest) + "," + "pos" + "\n")
	negbest = 100000000000
	temp_negbest = 99999999999
	gene = ""
	start = ""
	stop = ""
	motif = str(key)
	mstart = str(value)
	for row_1 in file1_data:
		#looking only at negative strand
		if row_1[3] == "neg":
			#calculating distance value (opposite from above)
			temp_negbest = value - int(row_1[1])
			#compare to best value found so far
			if temp_negbest < negbest and temp_negbest > 0:
				#reset best value
				negbest = temp_negbest
				gene = row_1[0]
				start = row_1[1]
				stop = row_1[2]
	file1.seek(0)
	#write to file
	file.write(motif + "," + mstart + "," + gene + "," + start + "," + stop + "," + str(negbest) + "," + "neg" + "\n")

		
