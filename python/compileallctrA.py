#to compile all information in one file

import csv

outfile1 = open('inprogressctrA.csv', 'w')
outfile2 = open('outputctrA.csv','w')

MM9 = open('MM9comp.csv','r')
MM9_data = csv.reader(MM9)

LBMC = open('LBMCcomp.csv', 'r')
LBMC_data = csv.reader(LBMC)

dist = open('distbetweenallctrA.csv', 'r')
dist_data = csv.reader(dist)


for row_MM9 in MM9_data:
	output = row_MM9[1]+','+ row_MM9[10]+','+ row_MM9[14]+ ','+'no' + '\n'
	for row_dist in dist_data:
		if row_MM9[1]==row_dist[2]:
			output = row_MM9[1]+','+ row_MM9[10]+','+ row_MM9[14]+ ','+'yes' +',' + row_dist[5] + '\n'
			break
	outfile1.write(output)
	dist.seek(0)

outfile1.close()
			
inprogress = open('inprogressctrA.csv','r')
inprogress_data = csv.reader(inprogress)


for row_inprogress in inprogress_data:
	for row_LBMC in LBMC_data:
		if row_inprogress[0]==row_LBMC[1]:
			try:
				outfile2.write(row_inprogress[0]+','+ row_inprogress[1]+','+ row_inprogress[2]+','+ row_LBMC[10]+','+ row_LBMC[14]+ ','+ row_inprogress[3] + ','+ row_inprogress[4] + '\n')
			except IndexError:
				outfile2.write(row_inprogress[0]+','+ row_inprogress[1]+','+ row_inprogress[2]+',' + row_LBMC[10]+','+ row_LBMC[14]+ ','+ ',' + '\n')
	LBMC.seek(0)


outfile2.close()

combined = open('outputctrA.csv', 'r')
combined_data = csv.reader(combined)

outfile3 = open('foranalysisctrA.csv','w')


secondorder = []
with open("secondordergenelist.txt") as f:
    for line in f:
       secondorder.append(line)
secondorder = [x.strip('\r\n') for x in secondorder]


for row_combined in combined_data:
	found = False
	for entry in secondorder:
		if row_combined[0]==entry:
			found = True
			output = row_combined[0] + ',' + row_combined[1] + ',' + row_combined[2] + ',' + row_combined[3] + ',' + row_combined[4] + ',' + row_combined[5] + ',' + row_combined[6] + ',' + 'no' + '\n'
	if found == False:
		output = row_combined[0] + ',' + row_combined[1] + ',' + row_combined[2] + ',' + row_combined[3] + ',' + row_combined[4] + ',' + row_combined[5] + ',' + row_combined[6]+ ',' + 'yes' + '\n'
	outfile3.write(output)

outfile3.close()