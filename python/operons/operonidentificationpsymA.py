##to read in all operon assignment files, and create single file that contains all operons identified


#read in LBMC, MM9 and wildtype operon predictions
with open('lbmc psymA.txt') as f:
	lbmc = f.readlines()
lbmc = [x.strip('\n') for x in lbmc]
lbmc = [x.strip('\r') for x in lbmc]

with open('mm9 psymA.txt') as f:
	mm9 = f.readlines()
mm9 = [x.strip('\n') for x in mm9]
mm9 = [x.strip('\r') for x in mm9]

with open('wt psymA.txt') as f:
	wt = f.readlines()
wt = [x.strip('\n') for x in wt]
wt = [x.strip('\r') for x in wt]

#combine sets to create a single set of all operon predictions
lbmcset = set(lbmc)
mm9set = set(mm9)
wtset = set(wt)
together = lbmcset.union(mm9set)
togetherset = set(together)
alltogether = togetherset.union(wtset)

#write out to new file
with open("psymAoperons.txt", "w") as outfile1:
	for item in alltogether:
		outfile1.write("%s\n" % item)
		
#read in combined file and split lines
operon=[]

with open('psymAoperons.txt', 'r') as f:
	for line in f:
		line = line.strip('\n')
		working = line.split('\t')
		sign = working[2]
		tempoperon = working[-1].split(', ')
		if sign=="+":
			tempoperon.pop(0)
		elif sign=='-':
			tempoperon.pop()
		operon.append(tempoperon)

#write operon out to new file where every gene entry is on a new line
with open('psymAsecondordergenes.txt','w') as outfile2:
	for item in operon:
		outfile2.write("\n".join(item))
		outfile2.write("\n")


#read file back in to list
with open('psymAsecondordergenes.txt','r') as f:
	secondorder = f.readlines()

secondorder = [x.strip('\n') for x in secondorder]

#read in annotation file that has gene name and gene synonym 
dict = {}
with open("NC_003037.ptt") as f:
    for line in f:
		line = line.strip('\n')
		working = line.split('\t')
		dictkey = working[5]
		dictval = working[4]
		(key, val) = dictkey, dictval
		dict[str(key)] = str(val)

#compare dictionary values to second order gene names, if match, output the gene synonym 		
output=[]
for key, val in dict.items():
	for item in secondorder:
		if str(val) == str(item):
			output.append(key)
		elif str(key) == str(item):
			output.append(key)

#remove duplicates from the final list
newoutput=[]
for i in output:
  if i not in newoutput:
    newoutput.append(i)

with open('psymAsecondordergenesfixed.txt', 'w') as outfile3:
	for item in newoutput:
		outfile3.write("%s\n" % item)
		
			
