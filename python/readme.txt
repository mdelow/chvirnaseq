##This is a general description and flow for the analysis used to
##map the chvI motif along the genome in S. meliloti and associate
##with the closest gene

Data Files
1) .gff files
	These are genome annotation files with the CDS, gene, and chvI
	motif matches in S. meliloti
	
2) psymAgenecoords.csv, psymBgenecoords.csv, chromgenecoords.csv
	These files contain the gene annotations extracted from the 
	.gff files, with four columns:
		i) gene name
		ii) gene start
		iii) gene stop
		iv) strand
		
3) psymAmotifcoord.txt, psymBmotifcoord.csv, chrommotifcoord.txt
	These files contain the motif annotations extracted from the
	.gff files, with two tab delimited entries:
		i) motif number (assigned chronologically based on occurrence 
						 in the genome)
		ii) motif start codon (motif analysis shows this is likely a
								bi-directional motif, but the 5' end 
								of the positive strand was chosen and
								used for consistency)
	In the .gff files, the motifs had different numbers assigned,
	based on their location in the original FIMO mapping. These numbers
	were replaced as described in 2)i), but the original motif numbers
	and the corresponding new numbers were conserved in the 'x' motif 
	legend.csv files in the 'useful files' folder.
	
Python Files
1) distnotate(psymA, psymB, chrom).py
	These files were used to indicate motifs that occur within a gene's
	CDS
		i) Input: 'x'genecoords.csv, 'x'motifcoords.txt
		ii) Output: motifingene'x'.txt -> contains three columns:
			a) motif number
			b) start 
			c) whether the motif is in a gene (yes/no)


	