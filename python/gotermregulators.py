#grab log2 and motif information for genes classified as transcriptional regulators

import csv

#read in file with gene, log2, and motif information
file1 = open("foranalysis.csv", "r")
file1_data = csv.reader(file1)

#bring in list of regulatory genes
regulators = []
with open("gotermoutput3.txt") as f:
    for line in f:
       regulators.append(line)
regulators = [x.strip('\n') for x in regulators]
regulators = [x.strip() for x in regulators]
#make outfile
outfile = open('regulators.csv', 'w')

#compare each gene in regulator list to the csv file and pull out information
for item in regulators:
	print item
	for row in file1_data:
		if item == str(row[0]):
			outfile.write(row[0] + ',' + row[1] + ',' + row[2] + ',' + row[3] + ',' + row[4] + ',' + row[5] + ',' + row[6] + ',' + row[7] + '\n')
	file1.seek(0)
outfile.close()