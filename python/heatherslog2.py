#get MM9 and LBMC log2 values associated with Heather's gene list used to determine chvI motif

import csv

outfile = open('heatherslog2.csv','w')


genes = []
with open("heathersgenes.txt") as f:
    for line in f:
       genes.append(line)
genes = [x.strip('\n') for x in genes]
genes = [x.strip() for x in genes]
	   
data = open('foranalysis.csv', 'r')
data_data = csv.reader(data)

for item in genes:
	for row in data_data:
		if item==str(row[0]):
			outfile.write(item + "," + row[1] +  "," + row[3] + "\n")
	data.seek(0)
		
outfile.close()