#to make note of case where a motif occurs in a gene

import csv

#makes new blank txt file
outfile = open("motifingenepsymBctrA.csv","w")

file1 = open("psymBgenecoords.csv", "r")


file1_data = csv.reader(file1)

d = {}
with open("psymBmotifcoordctrA.txt") as f:
    for line in f:
       (key, val) = line.split()
       d[int(key)] = int(val)
	   

for key,value in d.iteritems():
	motif = int(value)
	motifkey = str(key)
	motifvalue = str(value)
	output = motifkey + "," + motifvalue + "," + "no" + "\n"
	for row in file1_data:
		if row[3]=='neg':
			if motif <= int(row[1]) and motif >= int(row[2]):
				output = motifkey + "," + motifvalue + "," + "yes" + "\n"
				break
		if row[3]=='pos':
			if motif >= int(row[1]) and motif <= int(row[2]):
				output = motifkey + "," + motifvalue + "," + "yes" + "\n"
				break
	outfile.write(output)
	file1.seek(0)

outfile.close()
