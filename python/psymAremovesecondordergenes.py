#remove second order genes from gene coord files
import csv

#read in current gene file
file1 = open("psymAgenecoords.csv", "r")
file1_data = csv.reader(file1)

#bring in list of second order genes
secondorder = []
with open("psymAsecondordergenesfixed.txt") as f:
    for line in f:
       secondorder.append(line)
secondorder = [x.strip('\n') for x in secondorder]

#make outfile
outfile = open('psymAgenecoordsfinal.csv', 'w')

#compare each gene in gene file to second order gene list
for row in file1_data:
	match = False
	for item in secondorder:
		#if match is found, break from loop
		if item == str(row[0]):
			match = True
			break 
		output = row[0] + ',' + row[1] + ',' + row[2] + ',' + row[3] + '\n'
	if not match:
		outfile.write(output)

outfile.close()