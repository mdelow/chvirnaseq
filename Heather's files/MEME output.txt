********************************************************************************
MEME - Motif discovery tool
********************************************************************************
MEME version 4.10.0 (Release date: Wed May 21 10:35:36 2014 +1000)

For further information on how to interpret these results or to get
a copy of the MEME software please access http://meme.nbcr.net.

This file may be used as input to the MAST algorithm for searching
sequence databases for matches to groups of motifs.  MAST is available
for interactive use and downloading at http://meme.nbcr.net.
********************************************************************************


********************************************************************************
REFERENCE
********************************************************************************
If you use this program in your research, please cite:

Timothy L. Bailey and Charles Elkan,
"Fitting a mixture model by expectation maximization to discover
motifs in biopolymers", Proceedings of the Second International
Conference on Intelligent Systems for Molecular Biology, pp. 28-36,
AAAI Press, Menlo Park, California, 1994.
********************************************************************************


********************************************************************************
TRAINING SET
********************************************************************************
DATAFILE= all_of_em220FIMO.txt
ALPHABET= ACGT
Sequence name            Weight Length  Sequence name            Weight Length  
-------------            ------ ------  -------------            ------ ------  
SMa0723:+:AE006469:39174 1.0000    751  SMa5027:-:AE006469:34785 1.0000    354  
SMb20300:+:AL591985:3065 1.0000    338  SMb20809:+:AL591985:5935 1.0000    990  
SMb20998:+:AL591985:1233 1.0000    161  SMc00354:+:AL591688:3088 1.0000    178  
SMc00604:+:AL591688:1275 1.0000    364  SMc02560:-:AL591688:5214 1.0000    343  
SMc02833:+:AL591688:1819 1.0000    293  SMc02941:-:AL591688:2946 1.0000    193  
SMc03046:+:AL591688:7421 1.0000    273  SMc04352:-:AL591688:2236 1.0000    172  
SMb20946:-:AL591985:1173 1.0000    774  SMb20948:+:AL591985:1173 1.0000    745  
SMb20954:+:AL591985:1181 1.0000    140  AL591688:1965083:1965833 1.0000    751  
SMc04236:+:AL591688:2062 1.0000    448  AL591688:3573124:3573900 1.0000    777  
GELSHIFTED:AL591688:2517 1.0000     25  SMb21188:AL591985:937508 1.0000   1634  
SMb21440:+:shiftedAL5919 1.0000     94  BelCharplasmid:AL591688: 1.0000    137  
BelChar:SMa2103:AE006469 1.0000    220  AL591688:2203994:2204782 1.0000    789  
AL591688:689159:689306:+ 1.0000    148  AL591688:2991422:2991677 1.0000    256  
AL591688:1996706:1996863 1.0000    158  AL591688:2359114:2359313 1.0000    200  
AL591688:60821:60949:+   1.0000    129  AL591688:383802:383852:+ 1.0000     51  
AL591985:1634582:1634726 1.0000    145  
********************************************************************************

********************************************************************************
COMMAND LINE SUMMARY
********************************************************************************
This information can also be useful in the event you wish to report a
problem with the MEME software.

command: meme all_of_em220FIMO.txt -dna -oc . -nostatus -time 18000 -maxsize 60000 -mod zoops -nmotifs 1 -minw 6 -maxw 50 -revcomp 

model:  mod=         zoops    nmotifs=         1    evt=           inf
object function=  E-value of product of p-values
width:  minw=            6    maxw=           50    minic=        0.00
width:  wg=             11    ws=              1    endgaps=       yes
nsites: minsites=        2    maxsites=       31    wnsites=       0.8
theta:  prob=            1    spmap=         uni    spfuzz=        0.5
global: substring=     yes    branching=      no    wbranch=        no
em:     prior=   dirichlet    b=            0.01    maxiter=        50
        distance=    1e-05
data:   n=           12031    N=              31
strands: + -
sample: seed=            0    seqfrac=         1
Letter frequencies in dataset:
A 0.219 C 0.281 G 0.281 T 0.219 
Background letter frequencies (from dataset with add-one prior applied):
A 0.219 C 0.281 G 0.281 T 0.219 
********************************************************************************


********************************************************************************
MOTIF  1 MEME	width =  21  sites =  31  llr = 341  E-value = 1.9e-020
********************************************************************************
--------------------------------------------------------------------------------
	Motif 1 Description
--------------------------------------------------------------------------------
Simplified        A  52:1:12:521:::::13175
pos.-specific     C  222:a8452:2:361731932
probability       G  4417:122:212:251321::
matrix            T  :372::333678725234::3

         bits    2.2                      
                 2.0                      
                 1.8     *                
                 1.5     *                
Relative         1.3     *      **      * 
Entropy          1.1     *      **     ** 
(15.9 bits)      0.9   ****   * **  *  ** 
                 0.7   ****  ***** **  ***
                 0.4 * ****  ********  ***
                 0.2 * **** **************
                 0.0 ---------------------

Multilevel           AGTGCCCCATTTTCGCCTCAA
consensus            GT    TTT  GCTT GA CT
sequence                    G        T    
                                          
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
	Motif 1 sites sorted by position p-value
--------------------------------------------------------------------------------
Sequence name            Strand  Start   P-value                    Site       
-------------            ------  ----- ---------            ---------------------
GELSHIFTED:AL591688:2517     +      4  2.97e-09        GAG CGTGCCTTTTTTTCGCCACAA C         
SMb20948:+:AL591985:1173     -    214  1.66e-08 CCACTATATT AGCGCCCGCTTTTCTCTTCAA CCGGGAAACG
SMb20946:-:AL591985:1173     -    674  1.66e-08 CCACTATATT AGCGCCCGCTTTTCTCTTCAA CCGGGAAACG
SMc04236:+:AL591688:2062     -    282  1.94e-08 GGCGCCGAGA GCTGCCCGATTTTGGCCACAA GATACAATCG
SMb20954:+:AL591985:1181     +     45  1.94e-08 TTTCTTCGGG AGTGCCCTAGTTTCTCTTCCT GCGGGCTGAG
SMb20809:+:AL591985:5935     -    374  4.77e-08 AACTTCTGGC AATGCCGCATGTTCGCTACAA GAGCCTGATG
SMc03046:+:AL591688:7421     +     29  7.29e-08 TACCGCAGCC GCTGCCTCATTGTGGCGACAA TGCGGACAGA
AL591688:2203994:2204782     -    611  1.84e-07 AGAAGCGGTC CTTGCCACAATTTCGCTTCAC CTAACCGGCA
SMc02833:+:AL591688:1819     +      8  1.84e-07    TCCGGCC GGTACCCCTTTTTTGTCTCAA TGTTCTTTTT
AL591688:1965083:1965833     -    216  5.43e-07 GCCGTTAACC ATTTCCTCAATTTTTCTGCCT CCATTGCGCC
AL591688:1996706:1996863     -     55  8.48e-07 GTGACAGTTT ATTTCCTCATTTTTTCCCAAT AGATTTGTGG
SMb20998:+:AL591985:1233     +    109  1.17e-06 CAATCAAAGG ATTGCCGCAATTTCCGGACAA CTGCGACGAA
SMc00604:+:AL591688:1275     +    253  1.59e-06 TGGCTCTTGA GATTCCTCATTTCCTGATCAA TTTCGGGTCA
SMb21440:+:shiftedAL5919     +     44  1.76e-06 TGGTCAGCAA ATTGCCCCCTCGTCGCCGCAC CGAGCACTTC
SMa5027:-:AE006469:34785     -    190  2.15e-06 TGATGGCGAG GTCGCCATATATTCGCCCCAA AGAGAGACCA
SMb21188:AL591985:937508     -    154  4.17e-06 TCACGCCGTA AGTGCCGTTGTTTCCTATCAA AACGTCGGCG
BelCharplasmid:AL591688:     -     55  5.96e-06 TTTGGCTTGC ACTTCGCCTTTTCCTTGCCAA GATGGCGCCC
SMc02941:-:AL591688:2946     -    132  5.96e-06 CGGTCTACGG GCCGCCCTTTCTTTTTTACCA CACTTTGGCC
AL591688:2359114:2359313     -    164  7.70e-06 GTTTGTTTTT CGTGCACGATTTTCTCGTGCT GCCACATGTT
AL591688:3573124:3573900     +    195  8.37e-06 AAAACCCCTT CATGCGCGTTTTTTGTGACAC TATTATTTAC
BelChar:SMa2103:AE006469     +    114  9.87e-06 CGCACCCAAT GGTACCAGTATTTCTCCGCCT GATAGTATGC
AL591688:2991422:2991677     +    198  1.16e-05 GGCCGTCGAG ATTGCCGTCGTTTGGCGGCCC CCGATGAGAG
SMc04352:-:AL591688:2236     -     64  1.47e-05 TTGTTAGTAG ATGGCCATATCGCTGCCTCAT TTCGGGTTAT
SMc02560:-:AL591688:5214     +    204  1.58e-05 CCGATTAAAA AATGCATATTTTTCTTTAAAT CGATTAATAT
AL591688:60821:60949:+       +     66  1.84e-05 TCGACGGCGG CGCGCGTCATCGTCTCGTCCA TGGCGAGGTC
SMc00354:+:AL591688:3088     -     58  2.13e-05 GGAGTATTGC GGTGCACCATTTCTGCCGGAC CTCGCCACAT
SMa0723:+:AE006469:39174     -    297  2.47e-05 CGCGCCCCAC AATTCCAGCAATCCTCTTCAT CCAGGAGGCT
AL591688:689159:689306:+     +    104  3.04e-05 TGGCCATTCC AAGGCCGCTGTTCCGGGACCA CTGCGTGCTG
AL591688:383802:383852:+     +      4  3.48e-05        TTT GTTTCCTCCTCGCGGCCGCAT CCTTGCGCCG
SMb20300:+:AL591985:3065     -    108  7.84e-05 GCACATGATT GCGGCCTTTGGGTGGCGACAA CTCCATTGCA
AL591985:1634582:1634726     -     68  8.30e-05 TGACCGTATC GGCGCCCCAGCGCGTCGCCCA GACCATCTTC
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
	Motif 1 block diagrams
--------------------------------------------------------------------------------
SEQUENCE NAME            POSITION P-VALUE  MOTIF DIAGRAM
-------------            ----------------  -------------
GELSHIFTED:AL591688:2517            3e-09  3_[+1]_1
SMb20948:+:AL591985:1173          1.7e-08  213_[-1]_511
SMb20946:-:AL591985:1173          1.7e-08  673_[-1]_80
SMc04236:+:AL591688:2062          1.9e-08  281_[-1]_146
SMb20954:+:AL591985:1181          1.9e-08  44_[+1]_75
SMb20809:+:AL591985:5935          4.8e-08  373_[-1]_596
SMc03046:+:AL591688:7421          7.3e-08  28_[+1]_224
AL591688:2203994:2204782          1.8e-07  610_[-1]_158
SMc02833:+:AL591688:1819          1.8e-07  7_[+1]_265
AL591688:1965083:1965833          5.4e-07  215_[-1]_515
AL591688:1996706:1996863          8.5e-07  54_[-1]_83
SMb20998:+:AL591985:1233          1.2e-06  108_[+1]_32
SMc00604:+:AL591688:1275          1.6e-06  252_[+1]_91
SMb21440:+:shiftedAL5919          1.8e-06  43_[+1]_30
SMa5027:-:AE006469:34785          2.2e-06  189_[-1]_144
SMb21188:AL591985:937508          4.2e-06  153_[-1]_1460
BelCharplasmid:AL591688:            6e-06  54_[-1]_62
SMc02941:-:AL591688:2946            6e-06  131_[-1]_41
AL591688:2359114:2359313          7.7e-06  163_[-1]_16
AL591688:3573124:3573900          8.4e-06  194_[+1]_562
BelChar:SMa2103:AE006469          9.9e-06  113_[+1]_86
AL591688:2991422:2991677          1.2e-05  197_[+1]_38
SMc04352:-:AL591688:2236          1.5e-05  63_[-1]_88
SMc02560:-:AL591688:5214          1.6e-05  203_[+1]_119
AL591688:60821:60949:+            1.8e-05  65_[+1]_43
SMc00354:+:AL591688:3088          2.1e-05  57_[-1]_100
SMa0723:+:AE006469:39174          2.5e-05  296_[-1]_434
AL591688:689159:689306:+            3e-05  103_[+1]_24
AL591688:383802:383852:+          3.5e-05  3_[+1]_27
SMb20300:+:AL591985:3065          7.8e-05  107_[-1]_210
AL591985:1634582:1634726          8.3e-05  67_[-1]_57
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
	Motif 1 in BLOCKS format
--------------------------------------------------------------------------------
BL   MOTIF 1 width=21 seqs=31
GELSHIFTED:AL591688:2517 (    4) CGTGCCTTTTTTTCGCCACAA  1 
SMb20948:+:AL591985:1173 (  214) AGCGCCCGCTTTTCTCTTCAA  1 
SMb20946:-:AL591985:1173 (  674) AGCGCCCGCTTTTCTCTTCAA  1 
SMc04236:+:AL591688:2062 (  282) GCTGCCCGATTTTGGCCACAA  1 
SMb20954:+:AL591985:1181 (   45) AGTGCCCTAGTTTCTCTTCCT  1 
SMb20809:+:AL591985:5935 (  374) AATGCCGCATGTTCGCTACAA  1 
SMc03046:+:AL591688:7421 (   29) GCTGCCTCATTGTGGCGACAA  1 
AL591688:2203994:2204782 (  611) CTTGCCACAATTTCGCTTCAC  1 
SMc02833:+:AL591688:1819 (    8) GGTACCCCTTTTTTGTCTCAA  1 
AL591688:1965083:1965833 (  216) ATTTCCTCAATTTTTCTGCCT  1 
AL591688:1996706:1996863 (   55) ATTTCCTCATTTTTTCCCAAT  1 
SMb20998:+:AL591985:1233 (  109) ATTGCCGCAATTTCCGGACAA  1 
SMc00604:+:AL591688:1275 (  253) GATTCCTCATTTCCTGATCAA  1 
SMb21440:+:shiftedAL5919 (   44) ATTGCCCCCTCGTCGCCGCAC  1 
SMa5027:-:AE006469:34785 (  190) GTCGCCATATATTCGCCCCAA  1 
SMb21188:AL591985:937508 (  154) AGTGCCGTTGTTTCCTATCAA  1 
BelCharplasmid:AL591688: (   55) ACTTCGCCTTTTCCTTGCCAA  1 
SMc02941:-:AL591688:2946 (  132) GCCGCCCTTTCTTTTTTACCA  1 
AL591688:2359114:2359313 (  164) CGTGCACGATTTTCTCGTGCT  1 
AL591688:3573124:3573900 (  195) CATGCGCGTTTTTTGTGACAC  1 
BelChar:SMa2103:AE006469 (  114) GGTACCAGTATTTCTCCGCCT  1 
AL591688:2991422:2991677 (  198) ATTGCCGTCGTTTGGCGGCCC  1 
SMc04352:-:AL591688:2236 (   64) ATGGCCATATCGCTGCCTCAT  1 
SMc02560:-:AL591688:5214 (  204) AATGCATATTTTTCTTTAAAT  1 
AL591688:60821:60949:+   (   66) CGCGCGTCATCGTCTCGTCCA  1 
SMc00354:+:AL591688:3088 (   58) GGTGCACCATTTCTGCCGGAC  1 
SMa0723:+:AE006469:39174 (  297) AATTCCAGCAATCCTCTTCAT  1 
AL591688:689159:689306:+ (  104) AAGGCCGCTGTTCCGGGACCA  1 
AL591688:383802:383852:+ (    4) GTTTCCTCCTCGCGGCCGCAT  1 
SMb20300:+:AL591985:3065 (  108) GCGGCCTTTGGGTGGCGACAA  1 
AL591985:1634582:1634726 (   68) GGCGCCCCAGCGCGTCGCCCA  1 
//

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
	Motif 1 position-specific scoring matrix
--------------------------------------------------------------------------------
log-odds matrix: alength= 4 w= 21 n= 11411 bayes= 9.22781 E= 1.9e-020 
   114    -80     34  -1160 
   -18    -80     34     40 
 -1160    -54   -153    169 
  -176  -1160    140    -18 
 -1160    183  -1160  -1160 
  -118    152   -153  -1160 
   -44     46    -80     40 
  -276     79    -31     23 
   114    -54  -1160     56 
   -44  -1160    -54    156 
  -176    -54   -212    163 
 -1160  -1160    -31    182 
 -1160    -12  -1160    176 
 -1160    105    -54      4 
 -1160   -212     79    104 
 -1160    134   -153    -18 
  -176     20     20     40 
    56   -112    -54     69 
  -176    163   -212  -1160 
   169      5  -1160  -1160 
   132    -80  -1160     40 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
	Motif 1 position-specific probability matrix
--------------------------------------------------------------------------------
letter-probability matrix: alength= 4 w= 21 nsites= 31 E= 1.9e-020 
 0.483871  0.161290  0.354839  0.000000 
 0.193548  0.161290  0.354839  0.290323 
 0.000000  0.193548  0.096774  0.709677 
 0.064516  0.000000  0.741935  0.193548 
 0.000000  1.000000  0.000000  0.000000 
 0.096774  0.806452  0.096774  0.000000 
 0.161290  0.387097  0.161290  0.290323 
 0.032258  0.483871  0.225806  0.258065 
 0.483871  0.193548  0.000000  0.322581 
 0.161290  0.000000  0.193548  0.645161 
 0.064516  0.193548  0.064516  0.677419 
 0.000000  0.000000  0.225806  0.774194 
 0.000000  0.258065  0.000000  0.741935 
 0.000000  0.580645  0.193548  0.225806 
 0.000000  0.064516  0.483871  0.451613 
 0.000000  0.709677  0.096774  0.193548 
 0.064516  0.322581  0.322581  0.290323 
 0.322581  0.129032  0.193548  0.354839 
 0.064516  0.870968  0.064516  0.000000 
 0.709677  0.290323  0.000000  0.000000 
 0.548387  0.161290  0.000000  0.290323 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
	Motif 1 regular expression
--------------------------------------------------------------------------------
[AG][GT]TGCC[CT][CTG][AT]TT[TG][TC][CT][GT]C[CGT][TA]C[AC][AT]
--------------------------------------------------------------------------------




Time 23.07 secs.

********************************************************************************


********************************************************************************
SUMMARY OF MOTIFS
********************************************************************************

--------------------------------------------------------------------------------
	Combined block diagrams: non-overlapping sites with p-value < 0.0001
--------------------------------------------------------------------------------
SEQUENCE NAME            COMBINED P-VALUE  MOTIF DIAGRAM
-------------            ----------------  -------------
SMa0723:+:AE006469:39174         3.54e-02  296_[-1(2.47e-05)]_434
SMa5027:-:AE006469:34785         1.44e-03  189_[-1(2.15e-06)]_144
SMb20300:+:AL591985:3065         4.86e-02  107_[-1(7.84e-05)]_210
SMb20809:+:AL591985:5935         9.26e-05  373_[-1(4.77e-08)]_596
SMb20998:+:AL591985:1233         3.30e-04  108_[+1(1.17e-06)]_32
SMc00354:+:AL591688:3088         6.72e-03  57_[-1(2.13e-05)]_100
SMc00604:+:AL591688:1275         1.10e-03  252_[+1(1.59e-06)]_91
SMc02560:-:AL591688:5214         1.02e-02  203_[+1(1.58e-05)]_91_\
    [-1(9.28e-05)]_7
SMc02833:+:AL591688:1819         1.01e-04  7_[+1(1.84e-07)]_46_[-1(6.19e-05)]_\
    198
SMc02941:-:AL591688:2946         2.06e-03  131_[-1(5.96e-06)]_41
SMc03046:+:AL591688:7421         3.69e-05  28_[+1(7.29e-08)]_35_[+1(7.39e-05)]_\
    168
SMc04352:-:AL591688:2236         4.45e-03  63_[-1(1.47e-05)]_88
SMb20946:-:AL591985:1173         2.50e-05  673_[-1(1.66e-08)]_80
SMb20948:+:AL591985:1173         2.40e-05  213_[-1(1.66e-08)]_87_\
    [+1(4.83e-05)]_403
SMb20954:+:AL591985:1181         4.66e-06  44_[+1(1.94e-08)]_75
AL591688:1965083:1965833         7.94e-04  215_[-1(5.43e-07)]_515
SMc04236:+:AL591688:2062         1.66e-05  281_[-1(1.94e-08)]_146
AL591688:3573124:3573900         1.26e-02  50_[+1(5.14e-05)]_123_\
    [+1(8.37e-06)]_562
GELSHIFTED:AL591688:2517         2.97e-08  3_[+1(2.97e-09)]_1
SMb21188:AL591985:937508         1.34e-02  153_[-1(4.17e-06)]_1460
SMb21440:+:shiftedAL5919         2.61e-04  43_[+1(1.76e-06)]_30
BelCharplasmid:AL591688:         1.39e-03  54_[-1(5.96e-06)]_62
BelChar:SMa2103:AE006469         3.94e-03  113_[+1(9.87e-06)]_86
AL591688:2203994:2204782         2.83e-04  610_[-1(1.84e-07)]_158
AL591688:689159:689306:+         7.75e-03  103_[+1(3.04e-05)]_24
AL591688:2991422:2991677         5.46e-03  197_[+1(1.16e-05)]_38
AL591688:1996706:1996863         2.34e-04  54_[-1(8.48e-07)]_83
AL591688:2359114:2359313         2.77e-03  163_[-1(7.70e-06)]_16
AL591688:60821:60949:+           4.01e-03  65_[+1(1.84e-05)]_43
AL591688:383802:383852:+         2.16e-03  3_[+1(3.48e-05)]_27
AL591985:1634582:1634726         2.05e-02  67_[-1(8.30e-05)]_57
--------------------------------------------------------------------------------

********************************************************************************


********************************************************************************
Stopped because nmotifs = 1 reached.
********************************************************************************

CPU: compute-0-2.local

********************************************************************************